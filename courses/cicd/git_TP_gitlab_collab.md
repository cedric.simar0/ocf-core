title:GIT - TP Collab Gitlab (Bonus)
intro:nous permettra de collaborer sur Gitlab.
conclusion:Construit un projet collaboratif à l'aide de Gitlab

---

## Réécrivons l'histoire de l'informatique à l'aide du modèle de branches Git

- Tous ensemble : on découpe l'histoire de l'informatique en 5 parties.
- On définit une organisation (découpage du groupe, 1 sprint = 20 minutes par exemple)
- On forme 5 groupes
  - 4 travaillent sur leur période en utilisant le modèle de branche.
  - 1 travaille sur l'organisation globale du dépôt et la synchronisation des 4 autres
- A la fin de chaque sprint, on intègre les modifications, toujours en respectant le modèle de branche.