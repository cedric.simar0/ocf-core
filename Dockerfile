# Choix de l'image de base
FROM golang:1.20

# Mise à jour du système d'exploitation
RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y git npm

ENV export GOPATH=$HOME/go
ENV export PATH=$PATH:$GOPATH/bin

# Création du répertoire de travail
WORKDIR /app

# Copie de tout le contenu de l'API dans le conteneur
COPY . /app



RUN cd /app && go get golang.org/x/text/transform && go get golang.org/x/text/unicode/norm && go install github.com/swaggo/swag/cmd/swag@latest

RUN swag init --parseDependency --parseInternal


# Installation des dépendances Node.js
RUN npm install -g @marp-team/marp-core markdown-it-include markdown-it-container markdown-it-attrs 

# Exposition du port 8000
EXPOSE 8000

# Commande par défaut pour lancer l'application
CMD ["go", "run", "main.go"]
